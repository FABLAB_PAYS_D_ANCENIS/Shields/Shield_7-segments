#ifndef CLUMINOSITY_H
#define CLUMINOSITY_H

#include "Shield7Segments.h"
#include "LightDependentResistor.h"

class CLuminosity
{
public:
    CLuminosity();
    uint8 getValue();

private:
    LightDependentResistor *photocell;

};

#endif // CLUMINOSITY_H
