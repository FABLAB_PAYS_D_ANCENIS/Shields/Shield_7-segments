#ifndef CIRRECEIVE_H
#define CIRRECEIVE_H

#include "Shield7Segments.h"
#include "IRremote.h"

class CIrReceive
{
public:
    CIrReceive();
    bool isCodeReceived(void);
    int32 receiveCode(void);    

private:
    IRrecv *irrecv;
    decode_results results;
    uint32 u32Code;

};

#endif // CIRRECEIVE_H
