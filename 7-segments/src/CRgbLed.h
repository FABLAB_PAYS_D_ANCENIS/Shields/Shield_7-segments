#ifndef CRGBLED_H
#define CRGBLED_H

#include "generic.h"
#include "Adafruit_NeoPixel.h"


class CRgbLed
{
public:
    CRgbLed(uint8 u8PinLed, uint8 u8NbLed);
    void switchOnLed(uint8 u8Led, uint8 u8Color);
    void switchOffLed(uint8 u8Led);
    void switchOffAllLeds(void);
    void switchOnLedRgb(uint8 u8Led, uint8 u8R, uint8 u8G, uint8 u8B);
    void patternLed(uint8 u8Pattern);

private:
	uint8 m_u8PinLed;
	uint8 m_u8NbLed;
    Adafruit_NeoPixel *pixels;
};

#endif // CRGBLED_H
