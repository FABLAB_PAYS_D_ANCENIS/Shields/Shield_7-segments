#ifndef CBUZZER_H
#define CBUZZER_H

#include "generic.h"
#include "NewTone.h"

enum {DO, RE, MI, FA, SOL, LA, SI};

#define freqDO  131
#define freqRE  147
#define freqMI  165
#define freqFA  175
#define freqSOL 196
#define freqLA  220
#define freqSI  247

class CBuzzer
{
public:
	CBuzzer(uint8 u8PinBuzzer);
	void playNote(uint8 u8Note);
	void playTimedNote(uint8 u8Note, uint32 u32Time);
	void stopNote(void);
	void setOctave(uint8 u8Oct);

private:
	uint8 m_u8PinBuzzer;
	uint8 m_u8Octave;
	uint8 m_u8OctaveDef;
	static uint8 u8FreqTable[7];

};

#endif // CBUZZER_H
