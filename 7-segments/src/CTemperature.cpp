#include "CTemperature.h"

/* Temperature Law of thermistance
 *  10k @ 25°C
 *  B Coeff = 3950
 *  Accuracy: 5%
 *  Model: MF52AT
 *   R     T°    Error
 *  1000  87,72  26.14
 *  2000  66,23  11.40
 *  3000  54,80   5.69
 *  4000  47,15   2.94
 *  5000  41,46   1.50
 *  6000  36,96   0.72
 *  7000  33,25   0.29
 *  8000  30,11   0.06
 *  9000  27,39  -0.05
 * 10000  25,00  -0.10
 * 11000  22,87  -0.10
 * 12000  20,95  -0.09
 * 13000  19,21   0.07
 * 14000  17,62   0.04
 * 15000  16,15   0.03
 * 16000  14,79   0.01
 * 17000  13,52   0.01
 * 18000  12,33   0.02
 * 19000  11,22   0.04
 * 20000  10,18  -0.05
 * 25000   5,71  -0.27
 * 30000   2,17  -0.63
 * 40000  -3,24  -1.58
 * 50000  -7,30  -2.67
 * 60000 -10,52  -3.76 */


/*****************************************************************************
 * Function    : CTemperature                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CTemperature::CTemperature()
{
    // Setup pin
    pinMode(cPin7segTemperature, INPUT);
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
int8 CTemperature::getValue()
{
    // T = 0.08713x - 19.558 (x = voltage reading in samples (1024 range), 
    // with 5V alim and R = 10000)
    return ((((unsigned long)(analogRead(cPin7segTemperature)) * 871) / 1000) - 195) / 10;
}
