#include "CIrReceive.h"

/*****************************************************************************
 * Function    : CButton                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CIrReceive::CIrReceive()
{
    // Setup pin
    irrecv = new IRrecv(cPin7segIrReceive);

    // Start the receiver
    irrecv->enableIRIn(); 

    u32Code = 0;
}

     
/*****************************************************************************
 * Function    : iscodeReceived                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CIrReceive::isCodeReceived(void)
{
    uint32 u32Res = 0;
    
    if (irrecv->decode(&results)) 
    {
        // Get value
        u32Res = results.value;

        // Receive the next value
        irrecv->resume(); 

        // Check code
        if( u32Res == 4294967295)
        {
            u32Res  = 0;
            u32Code = 0;
            return false;
        }
        else
        {
            u32Code = u32Res;
            return true;
        }
    }  

    return false;
}

/*****************************************************************************
 * Function    : receiveCode                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
int32 CIrReceive::receiveCode(void) 
{
    return u32Code;
}
