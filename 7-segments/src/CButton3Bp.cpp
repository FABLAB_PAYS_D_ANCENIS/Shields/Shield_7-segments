#include "CButton3Bp.h"

/*****************************************************************************
 * Function    : CButton3Bp                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CButton3Bp::CButton3Bp()
{
    // Setup pin
    pinMode(cPin7segButtonUp,   INPUT);
    pinMode(cPin7segButtonDown, INPUT);
    pinMode(cPin7segButtonOk,   INPUT);

    // Init tempos
    u32TempoBpUp   = 0;
    u32TempoBpDown = 0;
    u32TempoBpOk   = 0;

    u32TempoBpUpEdge   = 0;
    u32TempoBpDownEdge = 0;
    u32TempoBpOkEdge   = 0;

    // Init states
    bBpUpState    = fabDigitalRead(cBpUp);
    bBpDownState  = fabDigitalRead(cBpDown);
    bBpOkstate    = fabDigitalRead(cBpOk);

	  // Init old state
    bBpUpOldState   = 0;
    bBpDownOldState = 0;
    bBpOkOldState   = 0;
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CButton3Bp::getEdgeValue(uint8 u8Button)
{
	bool bRes = 0;

    if(u8Button == 0)
    {
		if((millis() - u32TempoBpUpEdge) > 100)
        {
			bRes = fabDigitalRead(cBpUp);

            if(bBpUpOldState != bRes)
            {
                u32TempoBpUpEdge = millis();

				if(bRes > bBpUpOldState)
				{
					bBpUpOldState = bRes;
					return 1;
				}
            }

			bBpUpOldState = bRes;

            return 0;
        }
        else
        {
            return 0;
        }
    }
    else if(u8Button == 1)
    {
        if((millis() - u32TempoBpDownEdge) > 100)
        {
			bRes = fabDigitalRead(cBpDown);

            if(bBpDownOldState != bRes)
            {
                u32TempoBpDownEdge = millis();

				if(bRes > bBpDownOldState)
				{
					bBpDownOldState = bRes;
					return 1;
				}
            }

			bBpDownOldState = bRes;

            return 0;
        }
        else
        {
            return 0;
        }
    }
    else if(u8Button == 2)
    {
        if((millis() - u32TempoBpOkEdge) > 100)
        {
			bRes = fabDigitalRead(cBpOk);

            if(bBpOkOldState != bRes)
            {
                u32TempoBpOkEdge = millis();

				if(bRes > bBpOkOldState)
				{
					bBpOkOldState = bRes;
					return 1;
				}
            }

			bBpOkOldState = bRes;

            return 0;
        }
        else
        {
            return 0;
        }
    }

    return bRes;
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CButton3Bp::getValue(uint8 u8Button)
{
    bool bRes = 0;

    if(u8Button == 0)
    {
        if((millis() - u32TempoBpUp) > 100)
        {
            //Serial.println(bBpUpState);
            if(bBpUpState != fabDigitalRead(cBpUp))
            {
                bBpUpState = fabDigitalRead(cBpUp);
                u32TempoBpUp = millis();
            }

            return bBpUpState;
        }
        else
        {
            return bBpUpState;
        }
    }
    else if(u8Button == 1)
    {
        if((millis() - u32TempoBpDown) > 100)
        {
            if(bBpDownState != fabDigitalRead(cBpDown))
            {
                bBpDownState = fabDigitalRead(cBpDown);
                u32TempoBpDown = millis();
            }

            return bBpDownState;
        }
        else
        {
            return bBpDownState;
        }
    }
    else if(u8Button == 2)
    {
        if((millis() - u32TempoBpOk) > 100)
        {

            if(bBpOkstate != fabDigitalRead(cBpOk))
            {
                bBpOkstate = fabDigitalRead(cBpOk);
                u32TempoBpOk = millis();
            }

            return bBpOkstate;
        }
        else
        {
            return bBpOkstate;
        }
    }

    return bRes;
}

/*****************************************************************************
 * Function    : fabDigitalRead                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CButton3Bp::fabDigitalRead(uint8 u8Button)
{
    uint16 u16Val;

    if(m_u8Pcb7segRevision == 1)
    {
        if(u8Button == cBpOk)
            return digitalRead(cPin7segButtonOk);
        if(u8Button == cBpUp)
            return digitalRead(cPin7segButtonUp);
        if(u8Button == cBpDown)
            return digitalRead(cPin7segButtonDown);
    }
    else if(m_u8Pcb7segRevision == 2)
    {
        u16Val = analogRead(A0);

        if(u8Button == cBpOk)
        {
            return digitalRead(cPin7segButtonOk);
        }
        if(u8Button == cBpUp)
        {
            if(u16Val > 950) return 1;
            else return 0;
        }
        if(u8Button == cBpDown)
        {
            if(u16Val < 80) return 1;
            else return 0;
        }
    }
}
