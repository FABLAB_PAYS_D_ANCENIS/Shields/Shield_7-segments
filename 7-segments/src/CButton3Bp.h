#ifndef CBUTTON3BP_H
#define CBUTTON3BP_H

#include "CSystem7Seg.h"
#include "Shield7Segments.h"

#define cBpOk   0
#define cBpUp   1
#define cBpDown 2

class CButton3Bp
{
public:
    CButton3Bp();
    bool getValue(uint8 u8Button);
	bool getEdgeValue(uint8 u8Button);

private:
  uint32 u32TempoBpUp;
  uint32 u32TempoBpDown;
  uint32 u32TempoBpOk;

  uint32 u32TempoBpUpEdge;
  uint32 u32TempoBpDownEdge;
  uint32 u32TempoBpOkEdge;

  bool   bBpUpState;
  bool   bBpDownState;
  bool   bBpOkstate;

  bool   bBpUpOldState;
  bool   bBpDownOldState;
  bool   bBpOkOldState;

  bool   fabDigitalRead(uint8 u8Button);
};

#endif // CBUTTON3BP_H
