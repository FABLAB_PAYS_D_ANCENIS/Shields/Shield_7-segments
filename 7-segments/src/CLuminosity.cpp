#include "CLuminosity.h"

/*****************************************************************************
 * Function    : CLuminosity                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CLuminosity::CLuminosity()
{
    // Setup pin
    photocell = new LightDependentResistor (cPin7segLuminosity, 750, LightDependentResistor::GL5516);

    // Set photocell to ground
    photocell->setPhotocellPositionOnGround(true);
}

/*****************************************************************************
 * Function    : getValue                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
uint8 CLuminosity::getValue()
{
    float intensity_in_lux;
    float intensity_in_pct;
    
    // Get intensity inlux
    intensity_in_lux = photocell->getCurrentLux();

    // Convert it to %
    intensity_in_pct = 40*pow(intensity_in_lux,0.1);

    // Saturate to 100%
    if(intensity_in_pct > 100)
        intensity_in_pct = 100;

    // Saturate to 0%
    if(intensity_in_pct < 0)
        intensity_in_pct = 0;
        
    return (intensity_in_pct);
}
