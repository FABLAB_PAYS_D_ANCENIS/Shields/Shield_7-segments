#ifndef CAFF7SEG_H
#define CAFF7SEG_H

#include "Shield7Segments.h"
#include "SevenSegmentTM1637.h"

class CAff7Seg
{
public:
    CAff7Seg();
    void writeAllSegments(String s8Str);
    void writeAllSegments(int32 s32Val);
    void writeOneSegment(uint8 u8Seg, char s8Char);
    void writeOneSegment(uint8 u8Seg, char *s8Char);
    void clearAllSegments(void);
	void displayDots(bool bOn);
	void writeLineByLine(uint32 u32Lines);

private:
    SevenSegmentTM1637 *SevenSeg;
    String s8MemoStr;
    uint16 u16Memo;
};

#endif // CAFF7SEG_H
