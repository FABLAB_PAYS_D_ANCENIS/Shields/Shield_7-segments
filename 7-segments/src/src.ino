#include <SPI.h>
#include "CSystem7Seg.h"

// Classes
CRgbLed        *RgbLed;
CAff7Seg       *Aff7Seg;
CLuminosity    *Luminosity;
CTemperature   *Temperature;
CSimpleLed     *SimpleLed;
CHallEffect    *HallEffect;
CButton3Bp     *Button;
CPotentiometer *Potentiometer;
CIrReceive     *IrReceive;
CInclinometer  *Inclinometer;
CBuzzer        *Buzzer;
CRadioFreq     *RadioFreq;
CSystem7Seg    *System;

// Variables
uint32 u32Count;
uint32 u32Tempo;
uint32 u32Tmp;
uint32 u32Counter;
bool   bOldBpUp;
bool   bOldBpDown;
bool   bOldBpOk;
uint8  u8MainStep;
uint8  u8AuxStep;
uint8  u8ColorR, u8ColorG, u8ColorB;

// Enums
enum {cStep7Seg,
      cStepLedSimple,
      cStepLedRgb,
      cStepBuzzer,
      cStepHallEffect,
      cStepIrRx,
      cStepInclino,
      cStepTemp,
      cStepLum,
      cStepBp,
      cStepPotar,
      cStepRfTx,
      cStepRfRx
      };

/*****************************************************************************
 * Function    : setup                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void setup()
{
    // Serial
    // Serial.begin(9600);
    // If serial.begin is activated, bp Ok will not work anymore.

    // Instanciate classes
    System    = new CSystem7Seg();
    RgbLed = new CRgbLed(7,10);
    Aff7Seg = new CAff7Seg();
    Luminosity = new CLuminosity();
    Temperature = new CTemperature();
    SimpleLed = new CSimpleLed();
    HallEffect = new CHallEffect();
    Button = new CButton3Bp();
    Potentiometer = new CPotentiometer();
    IrReceive = new CIrReceive();
    Inclinometer = new CInclinometer();
    Buzzer = new CBuzzer(3);
     RadioFreq = new CRadioFreq(4,10);

    System->disableErrors();

    // Init variables
    u32Count = 0;
    bOldBpUp = 0;
    u8MainStep = cStep7Seg;
    u8AuxStep = 0;
    u32Counter = 0;
    u32Tempo = millis();
}

/*****************************************************************************
 * Function    : loop                                                        *
 *...........................................................................*
 * Description : The loop function runs over and over again forever          *
 *****************************************************************************/
void loop()
{
    int32  s32Tmp;
    uint16 u16Tmp;
    int16  s16Tmp;
    uint32 u32Tmp;

    System->check();

    // Step machine
    switch(u8MainStep)
    {
        /* ----------------------  7 Segements -----------------------------*/
        case cStep7Seg:
            Aff7Seg->writeAllSegments(u32Counter);
            if((millis() - u32Tempo) > 500)
            {
                u32Counter += 111;
                Aff7Seg->writeAllSegments(u32Counter);
                Aff7Seg->writeAllSegments(s32Tmp);
                Aff7Seg->displayDots(true);
                u32Tempo = millis();
            }
        break;

        /* ----------------------  Simple Leds -----------------------------*/
        case cStepLedSimple:
            Aff7Seg->writeAllSegments("LED");
            SimpleLed->init();
            if((millis() - u32Tempo) > 300)
            {
                switch(u8AuxStep)
                {
                    case 0:
                        SimpleLed->invertLed(0);
                    break;
                    case 1:
                        SimpleLed->invertLed(1);
                    break;
                    case 2:
                        SimpleLed->invertLed(2);
                    break;
                    default:
                        u8AuxStep = 0;
                    break;
                }

                 if(u8AuxStep < 2)
                   u8AuxStep++;
                 else
                   u8AuxStep = 0;

                u32Tempo = millis();
            }
        break;

        /* ------------------------  Leds RGB -----------------------------*/
        case cStepLedRgb:
            Aff7Seg->writeAllSegments("RGB");
            if((millis() - u32Tempo) > 50)
            {
                RgbLed->switchOnLedRgb(u8AuxStep+1, u8ColorR, u8ColorG, u8ColorB);

                if(u8AuxStep < 9)
                {
                   u8AuxStep++;
                }
                else
                {
                   u8AuxStep = 0;
                   u8ColorR  = random(0,2)*10;
                   u8ColorG  = random(0,2)*10;
                   u8ColorB  = random(0,2)*10;

                   while(u8ColorR == 0 && u8ColorG == 0 && u8ColorB == 0)
                   {
                       u8ColorR  = random(0,2)*10;
                       u8ColorG  = random(0,2)*10;
                       u8ColorB  = random(0,2)*10;
                   }
                }

                u32Tempo = millis();
            }
        break;

        /* -------------------------  Buzzer -----------------------------*/
        case cStepBuzzer:
            Aff7Seg->writeAllSegments("BUZZ");
            if((millis() - u32Tempo) > 300)
            {
                Buzzer->playNote(random(0,6));

                u32Tempo = millis();
            }
        break;

        /* ----------------------  Hall Effect ---------------------------*/
        case cStepHallEffect:
            if(u8AuxStep == 0)
            {
                Aff7Seg->writeAllSegments("HALL");
                if((millis() - u32Tempo) > 700)
                {
                  u8AuxStep = 1;
                  Aff7Seg->writeAllSegments("    ");
                  //Serial.println("HALL");
                }
            }
            else
            {
                if(HallEffect->getValue() == 0)
                {
                    Aff7Seg->writeAllSegments(" ON ");
                }
                else
                {
                    Aff7Seg->writeAllSegments(" OFF");
                }
            }

        break;

        /* ---------------- Infra-rouge receive --------------------------*/
        case cStepIrRx:
            if(u8AuxStep == 0)
            {
                Aff7Seg->writeAllSegments("IRRX");
                if((millis() - u32Tempo) > 700)
                {
                  u8AuxStep = 1;
                  Aff7Seg->writeAllSegments("    ");
                  //Serial.println("IRRX");
                }
            }
            else
            {
                if((millis() - u32Tempo) > 100)
                {
                    if(IrReceive->isCodeReceived())
                    {
                        u32Tmp = IrReceive->receiveCode();
                        Aff7Seg->writeAllSegments(String(u32Tmp));
                        Serial.println(u32Tmp);
                    }

                    u32Tempo = millis();
                }
            }

        break;

        /* --------------------  Inclinometer ----------------------------*/
        case cStepInclino:
            if(u8AuxStep == 0)
            {
                Aff7Seg->writeAllSegments("INCL");
                if((millis() - u32Tempo) > 700)
                {
                  u8AuxStep = 1;
                  Aff7Seg->writeAllSegments("    ");
                  //Serial.println("INCL");
                }
            }
            else
            {
                if((millis() - u32Tempo) > 100)
                {
                    if(Inclinometer->getValue() == 1)
                        Aff7Seg->writeAllSegments(" ON ");
                    else
                        Aff7Seg->writeAllSegments(" OFF");
                    u32Tempo = millis();
                }
            }

        break;

        /* --------------------  Temperature -----------------------------*/
        case cStepTemp:
            if(u8AuxStep == 0)
            {
                Aff7Seg->writeAllSegments("TEMP");
                if((millis() - u32Tempo) > 700)
                {
                  u8AuxStep = 1;
                  Aff7Seg->writeAllSegments("    ");
                  //Serial.println("TEMP");
                }
            }
            else
            {
                if((millis() - u32Tempo) > 500)
                {
                    Aff7Seg->writeAllSegments(Temperature->getValue());
                    //Aff7Seg->writeOneSegment(2,' ');
                    //Aff7Seg->writeOneSegment(3,'C');
                    u32Tempo = millis();
                }
            }

        break;

        /* ---------------------  Luminosity -----------------------------*/
        case cStepLum:
            if(u8AuxStep == 0)
            {
                Aff7Seg->writeAllSegments("LUM");
                if((millis() - u32Tempo) > 700)
                {
                  u8AuxStep = 1;
                  Aff7Seg->writeAllSegments("    ");
                  //Serial.println("LUM");
                }
            }
            else
            {
                if((millis() - u32Tempo) > 500)
                {
                    Aff7Seg->writeOneSegment(2,' ');
                    Aff7Seg->writeAllSegments(Luminosity->getValue());
                    u32Tempo = millis();
                }
            }

        break;

        /* -----------------------  Buttons ------------------------------*/
        case cStepBp:
            Aff7Seg->writeAllSegments("BP");

            if(Button->getValue(1) == 1)
            {
                Aff7Seg->writeOneSegment(3,'1');
            }
            else if(Button->getValue(2) == 1)
            {
                Aff7Seg->writeOneSegment(3,'2');
            }
            else
            {
                Aff7Seg->writeOneSegment(3,' ');
            }

        break;

        /* --------------------  Potentiometer ---------------------------*/
        case cStepPotar:
            if(u8AuxStep == 0)
            {
                Aff7Seg->writeAllSegments("POT");
                if((millis() - u32Tempo) > 700)
                {
                  u8AuxStep = 1;
                  Aff7Seg->writeAllSegments("    ");
                  //Serial.println("POT");
                }
            }
            else
            {
                if((millis() - u32Tempo) > 200)
                {
                    Aff7Seg->writeOneSegment(2,' ');
                    Aff7Seg->writeAllSegments(Potentiometer->getValue());
                    u32Tempo = millis();
                }
            }

        break;

        /* ------------------  Radio-frequency ---------------------------*/
        case cStepRfTx:
            if(u8AuxStep == 0)
            {
                Aff7Seg->writeAllSegments("RFTX");
                if((millis() - u32Tempo) > 700)
                {
                    u8AuxStep = 1;
                    Aff7Seg->writeAllSegments("    ");
                    Serial.println("RFTX");
                    RadioFreq->init(1);
                    RadioFreq->destAddress(0);

                    s16Tmp = random(0,10999)-1000;
                    Aff7Seg->writeAllSegments(s16Tmp);
                    RadioFreq->send(s16Tmp, Potentiometer->getValue());
                    Serial.println(Potentiometer->getValue());
                    u32Tempo = millis();
                }
            }
            else
            {
                if((millis() - u32Tempo) > 3000)
                {
                    s16Tmp = random(0,10999)-1000;
                    Aff7Seg->writeAllSegments(s16Tmp);
                    RadioFreq->send(s16Tmp, Potentiometer->getValue());
                    u32Tempo = millis();
                }
            }

        break;

        /* ------------------  Radio-frequency ---------------------------*/
        case cStepRfRx:
            if(u8AuxStep == 0)
            {
                Aff7Seg->writeAllSegments("RFRX");
                if((millis() - u32Tempo) > 700)
                {
                  u8AuxStep = 1;
                  Aff7Seg->writeAllSegments("    ");
                  RadioFreq->init(0);
                  RadioFreq->destAddress(1);
                  //Serial.println("RFRX");
                }
            }
            else
            {
                if((millis() - u32Tempo) > 500)
                {
                    if(RadioFreq->isCodeReceived())
                    {
                      s32Tmp = RadioFreq->receiveCode();
                      Aff7Seg->writeAllSegments(s32Tmp);
                    }
                    u32Tempo = millis();
                }
            }

        break;

        /* ------------------------- Default ----------------------------*/
        default:
            // Init variables
            u32Count = 0;
            bOldBpUp = 0;
            u8MainStep = cStep7Seg;
            u8AuxStep = 0;
            u32Tmp = 0;
            u32Tempo = millis();
            RadioFreq->stop();
        break;
    }

    /* ---------------- Increment step machine ------------------------*/
    if((Button->getValue(0) == 1) && (bOldBpUp == 0))
    {
        u8MainStep++;
        u32Tempo = millis();
        u32Tmp = 0;
        u8AuxStep = 0;

        // Stop all devices
        Aff7Seg->clearAllSegments();
        Aff7Seg->displayDots(false);
        SimpleLed->switchOffLed(0);
        SimpleLed->switchOffLed(1);
        SimpleLed->switchOffLed(2);
        RgbLed->switchOffAllLeds();
        Buzzer->stopNote();
    }
    bOldBpUp = Button->getValue(0);
    bOldBpDown = Button->getValue(1);
    bOldBpOk = Button->getValue(2);
}
