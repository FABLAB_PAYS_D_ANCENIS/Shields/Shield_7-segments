#include "CSystem7Seg.h"

char m_u8Pcb7segRevision;

int cPin7segLuminosity = A3;
int cPin7segTemperature = A4;
int cPin7segPotentiometer = A5;

int cPinBuzzer = 3;

int cPin7segSimpleLedRed = 11;
int cPin7segSimpleLedOrange = 12;
int cPin7segSimpleLedGreen = 13;

int cPin7segHallEffect = 0;
int cPin7segIrTransmit = 1;
int cPin7segIrReceive = 2;
int cPin7segButtonUp = A2;
int cPin7segButtonDown = A0;
int cPin7segButtonOk = A1;

int cPin7SegClock = 8;
int cPin7SegDio = 9;

int cPin7segRfChipEnable = 4;
int cPin7segRfChipSelect = 10;
int cPin7segRfMiso = 12;
int cPin7segRfMosi = 11;
int cPin7segRfClock = 13;
int cPin7segRfIrq = 2;

int cPin7segInclinometer = 5;
int cPin7segRgbLed = 7;

/*****************************************************************************
 * Function    : CSystem7Seg                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CSystem7Seg::CSystem7Seg()
{
    // Init pcb rev to 0 = unknown
    m_u8Pcb7segRevision = 0;
    bErrorDisabled  = false;

    // Detect Pcb
    configure();    
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystem7Seg::configure(void)
{
    uint16 u16AnaValue = analogRead(A0);

    // Detect Rev 2 with VBatt/2 potential (1024/2)
    if((u16AnaValue > 460) && (u16AnaValue < 560))
    {
        m_u8Pcb7segRevision = 2;
    }
    else
    {
        m_u8Pcb7segRevision = 1;
    }

    // Select correct pins on shield rev 2
    if(m_u8Pcb7segRevision == 2)
    {
        cPin7segSimpleLedRed  = 6;    // 11 on Rev 1
        cPin7segTemperature   = A3;   // A4 on Rev 1
        cPin7segLuminosity    = A2;   // A3 on Rev 1
        cPin7segPotentiometer = A1;   // A5 on Rev 1
        cPin7segButtonOk      = 1;    // A1 on Rev 1
    }
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystem7Seg::disableErrors(void)
{
    bErrorDisabled = true;
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystem7Seg::check(void)
{
    // Check if error is disabled
    if(bErrorDisabled == true)
        return;

    // On Rev 1 pcb
    if(m_u8Pcb7segRevision == 1)
    {
        // Choose Led usage or RF usage
        if((m_bSimpleLedUSed == true) && (m_bRadioFreqUSed == true))
        {
            error(1);
        }
    }
    // On Rev 2 pcb
    else if(m_u8Pcb7segRevision == 2)
    {
        // Do not use RF with led green or orange (red led can be used)
        if(((m_bSimpleLedGreenUSed == true) || (m_bSimpleLedOrangeUSed == true)) && (m_bRadioFreqUSed == true))
        {
            error(2);
        }
    }
}

/*****************************************************************************
 * Function    : configure                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSystem7Seg::error(uint8 u8Err)
{
    CAff7Seg *Aff7Seg;
    Aff7Seg = new CAff7Seg();

    if(u8Err == 1)
    {
        Aff7Seg->writeAllSegments("ERR1");
    }
    else if(u8Err == 2)
    {
        Aff7Seg->writeAllSegments("ERR2");
    }

    // Stop processing
    while(1){}
}
