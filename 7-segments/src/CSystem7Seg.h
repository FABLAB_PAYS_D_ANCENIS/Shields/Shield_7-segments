#ifndef CSYSTEM7SEG_H
#define CSYSTEM7SEG_H

#include "CAff7Seg.h"

extern char m_u8Pcb7segRevision;

// Pin definition
extern int cPin7segLuminosity;
extern int cPin7segTemperature;
extern int cPin7segPotentiometer;

extern int cPin7segBuzzer;

extern int cPin7segSimpleLedRed;
extern int cPin7segSimpleLedOrange;
extern int cPin7segSimpleLedGreen;

extern int cPin7segHallEffect;
extern int cPin7segIrTransmit;
extern int cPin7segIrReceive;
extern int cPin7segButtonUp;
extern int cPin7segButtonDown;
extern int cPin7segButtonOk;

extern int cPin7SegClock;
extern int cPin7SegDio;

extern int cPin7segRfChipEnable;
extern int cPin7segRfChipSelect;
extern int cPin7segRfMiso;
extern int cPin7segRfMosi;
extern int cPin7segRfClock;
extern int cPin7segRfIrq;

extern int cPin7segInclinometer;
extern int cPin7segRgbLed;

#define cNUMRgbLeb            10

// Types definition
typedef signed char         int8;
typedef unsigned char       uint8;
typedef short               int16;
typedef unsigned short      uint16;
typedef long                int32;
typedef unsigned long       uint32;
typedef unsigned long long  uint64;


class CSystem7Seg
{
public:
    
    CSystem7Seg();
    void configure(void);
    void check(void);
    void disableErrors(void);
	void error(uint8 u8Err);
   

private:
    bool bErrorDisabled;
};

#endif // CSYSTEM_H
