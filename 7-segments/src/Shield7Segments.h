#include "Arduino.h"
#include "CRgbLed.h"
#include "CAff7Seg.h"
#include "CLuminosity.h"
#include "CTemperature.h"
#include "CSimpleLed.h"
#include "CHallEffect.h"
#include "CButton3Bp.h"
#include "CPotentiometer.h"
#include "CIrReceive.h"
#include "CInclinometer.h"
#include "CBuzzer.h"
#include "CRadioFreq.h"
#include "CSystem7Seg.h"

#define cNUMRgbLeb            10

// Types definition
typedef signed char         int8;
typedef unsigned char       uint8;
typedef short               int16;
typedef unsigned short      uint16;
typedef long                int32;
typedef unsigned long       uint32;
typedef unsigned long long  uint64;
