#include "CRgbLed.h"

/*****************************************************************************
 * Function    : CRgbLed                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CRgbLed::CRgbLed(uint8 u8PinLed, uint8 u8NbLed)
{
  m_u8PinLed = u8PinLed;
  m_u8NbLed = u8NbLed;
  pixels = new Adafruit_NeoPixel(m_u8NbLed, m_u8PinLed, NEO_GRB + NEO_KHZ800); // When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
  pixels->begin();
  for(int i = 0 ; i < m_u8NbLed ; i++)
  {
    pixels->setPixelColor(i, pixels->Color(0,0,0)); // Shutdown all leds.
    pixels->show(); // This sends the updated pixel color to the hardware.
  }
}

/*****************************************************************************
 * Function    : switchOnLed                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLed::switchOnLed(uint8 u8Led, uint8 u8Color)
{
  /*
  List of colors availables
  color == 0 -> Red
  color == 1 -> Orange
  color == 2 -> Green
  color == 3 -> Yellow
  color == 4 -> Blue
  color == 5 -> Indigo
  color == 6 -> Purple
  color == "other" -> White
  */

  if(u8Led > 0)
  {
      u8Led--;
  }

  if (u8Color == 0)
  {
   pixels->setPixelColor(u8Led, pixels->Color(10,0,0)); // Setcolor to red.
  }
  else if (u8Color == 1)
  {
   pixels->setPixelColor(u8Led, pixels->Color(10,5,0)); // Setcolor to orange.
  }
  else if (u8Color == 2)
  {
   pixels->setPixelColor(u8Led, pixels->Color(0,10,0)); // Setcolor to green.
  }
  else if (u8Color == 3)
  {
   pixels->setPixelColor(u8Led, pixels->Color(10,10,0)); // Setcolor to yellow.
  }
  else if (u8Color == 4)
  {
   pixels->setPixelColor(u8Led, pixels->Color(0,0,10)); // Setcolor to blue.
  }
  else if (u8Color == 5)
  {
   pixels->setPixelColor(u8Led, pixels->Color(5,0,10)); // Setcolor to indigo.
  }
  else if (u8Color == 6)
  {
   pixels->setPixelColor(u8Led, pixels->Color(10,0,10)); // Setcolor to purple.
  }
  else
  {
   pixels->setPixelColor(u8Led, pixels->Color(10,10,10)); // Setcolor to white.
  }

  pixels->show(); // This sends the updated pixel color to the hardware.
}

/*****************************************************************************
 * Function    : switchOffLed                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLed::switchOffLed(uint8 u8Led)
{
  if(u8Led > 0)
      u8Led--;

  pixels->setPixelColor(u8Led, pixels->Color(0,0,0)); // Shutdown led.
  pixels->show(); // This sends the updated pixel color to the hardware.
}

/*****************************************************************************
 * Function    : switchOffAllLeds                                            *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLed::switchOffAllLeds(void)
{
  for(int i = 0 ; i < m_u8NbLed ; i++)
  {
    pixels->setPixelColor(i, pixels->Color(0,0,0)); // Shutdown all leds.
  }
  pixels->show(); // This sends the updated pixel color to the hardware.
}

/*****************************************************************************
 * Function    : switchOnLedRgb                                              *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLed::switchOnLedRgb(uint8 u8Led, uint8 u8R, uint8 u8G, uint8 u8B)
{
  if(u8Led > 0)
      u8Led--;

  pixels->setPixelColor(u8Led, pixels->Color(u8R,u8G,u8B)); // Setcolor to specific color.
  pixels->show();
}


/*****************************************************************************
 * Function    : patternLed01                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CRgbLed::patternLed(uint8 u8Pattern)

/*
List of patterns availables
Pattern == 1 -> Success
Pattern == 2 -> Wrong answer
Pattern == 3 -> Rainbow
Pattern == 4 -> K2000
Pattern == 5 -> French police
Pattern == 6 -> Safety car
Pattern == "other" -> Randomm
*/

{
  if (u8Pattern == 1) // Setpattern "Success"
  {
    for(int i = 0 ; i < m_u8NbLed ; i++)
      {
        pixels->setPixelColor(i, pixels->Color(0,255,0));
      }
      pixels->show();

  }
  else if (u8Pattern == 2) // Setpattern "Wrong answer"
  {
    for(int i = 0 ; i < 5 ; i++)
      {
      for(int i = 0 ; i < m_u8NbLed ; i++)
        {
          pixels->setPixelColor(i, pixels->Color(0,0,0)); // Shutdown all leds.
        }
        pixels->show();
      delay(i*100);
      for(int i = 0 ; i < m_u8NbLed ; i++)
        {
          pixels->setPixelColor(i, pixels->Color(255,0,0));
        }
        pixels->show();
      delay(i*100);
    }
  }
  else if (u8Pattern == 3) // Setpattern "Rainbow"
  {
    for(int i = 0 ; i < m_u8NbLed ; i++)
      {
        pixels->setPixelColor(i, pixels->Color(0,0,0)); // Shutdown all leds.
      }
      pixels->show();

    pixels->setPixelColor(1, pixels->Color(255,0,0)); // Setcolor to red.
    pixels->setPixelColor(2, pixels->Color(255,127,0)); // Setcolor to orange.
    pixels->setPixelColor(3, pixels->Color(255,255,0)); // Setcolor to yellow.
    pixels->setPixelColor(4, pixels->Color(0,255,0)); // Setcolor to green.
    pixels->setPixelColor(5, pixels->Color(22,204,251)); // Setcolor to blue.
    pixels->setPixelColor(6, pixels->Color(0,0,255)); // Setcolor to indigo.
    pixels->setPixelColor(7, pixels->Color(255,0,255)); // Setcolor to purple.
    pixels->setPixelColor(8, pixels->Color(255,255,255)); // Setcolor to white.
    pixels->show(); // This sends the updated pixel color to the hardware.
  }
  else if (u8Pattern == 4) // Setpattern "K2000"
  {
    for(int i = 0 ; i < m_u8NbLed ; i++)
      {
        pixels->setPixelColor(i, pixels->Color(0,0,0)); // Shutdown all leds.
      }
      pixels->show();

    for(int i = 0 ; i < m_u8NbLed ; i++)
      {
        pixels->setPixelColor(i-1, pixels->Color(0,0,0));
        pixels->setPixelColor(i, pixels->Color(100,0,0));
        pixels->setPixelColor(i+1, pixels->Color(200,0,0));
        pixels->setPixelColor(i+2, pixels->Color(255,0,0));
        pixels->show();
        delay(100);
      }
      for(int i = m_u8NbLed ; i > 0 ; i--)
      {
        pixels->setPixelColor(i+1, pixels->Color(0,0,0));
        pixels->setPixelColor(i, pixels->Color(100,0,0));
        pixels->setPixelColor(i-1, pixels->Color(200,0,0));
        pixels->setPixelColor(i-2, pixels->Color(255,0,0));
        pixels->show();
        delay(100);
      }
  }
  else if (u8Pattern == 5) // Setpattern "French police"
  {
    for(int i = 0 ; i < m_u8NbLed ; i++)
      {
        pixels->setPixelColor(i, pixels->Color(0,0,0)); // Shutdown all leds.
      }
      pixels->show();

    for(int i = 0 ; i < m_u8NbLed+1 ; i++)
      {
        pixels->setPixelColor((i-1) % 10, pixels->Color(0,0,0));
        pixels->setPixelColor(i % 10, pixels->Color(0,0,255));
        pixels->setPixelColor((i+1) % 10, pixels->Color(0,0,255));
        pixels->setPixelColor((i+2) % 10, pixels->Color(0,0,255));
        pixels->show();
        delay(100);
      }
  }
  else if (u8Pattern == 6) // Setpattern "Savety car"
  {
    for(int i = 0 ; i < m_u8NbLed ; i++)
      {
      for(int i = 0 ; i < m_u8NbLed ; i++)
        {
          pixels->setPixelColor(i, pixels->Color(255,102,0));
        }
        pixels->show();
        delay(50);

      for(int i = 0 ; i < m_u8NbLed ; i++)
        {
          pixels->setPixelColor(i, pixels->Color(0,0,0));
        }
        pixels->show();
        delay(50);
      }
        delay(400);
  }
  else
  {
    long randNumber1;
    long randNumber2;
    long randNumber3;

    randNumber1 = random(0, 255);
    randNumber2 = random(0, 255);
    randNumber3 = random(0, 255);

    for(int i = 0 ; i < m_u8NbLed ; i++)
    {
      pixels->setPixelColor(i, pixels->Color(randNumber1,randNumber2,randNumber3));
      pixels->show();
      delay(50);
    }
  }
}
