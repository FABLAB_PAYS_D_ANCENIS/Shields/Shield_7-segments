#include "CSimpleLed.h"

char m_bSimpleLedUSed = false;
char m_bSimpleLedGreenUSed = false;
char m_bSimpleLedOrangeUSed = false;
char m_bSimpleLedRedUSed = false;

/*****************************************************************************
 * Function    : CSimpleLed                                                  *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CSimpleLed::CSimpleLed()
{
    // Setup pin
    pinMode(cPin7segSimpleLedRed,    OUTPUT);
    pinMode(cPin7segSimpleLedOrange, OUTPUT);
    pinMode(cPin7segSimpleLedGreen,  OUTPUT);

    m_bSimpleLedUSed = true;
}

/*****************************************************************************
 * Function    : CSimpleLed                                                  *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSimpleLed::init(void)
{
    // Setup pin
    pinMode(cPin7segSimpleLedRed,    OUTPUT);
    pinMode(cPin7segSimpleLedOrange, OUTPUT);
    pinMode(cPin7segSimpleLedGreen,  OUTPUT);
}

/*****************************************************************************
 * Function    : switchOnLed                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSimpleLed::switchOnLed(uint8 u8Led)
{
    if(u8Led == 0)
    {
        m_bSimpleLedRedUSed = true;
        digitalWrite(cPin7segSimpleLedRed, true);
    }
    else if(u8Led == 1)
    {
        m_bSimpleLedOrangeUSed = true;
        digitalWrite(cPin7segSimpleLedOrange, true);
    }
    else if(u8Led == 2)
    {
        m_bSimpleLedGreenUSed = true;
        digitalWrite(cPin7segSimpleLedGreen, true);
    }
}

/*****************************************************************************
 * Function    : switchOffLed                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSimpleLed::switchOffLed(uint8 u8Led)
{
    if(u8Led == 0)
    {
        m_bSimpleLedRedUSed = true;
        digitalWrite(cPin7segSimpleLedRed, false);
    }
    else if(u8Led == 1)
    {
        m_bSimpleLedOrangeUSed = true;
        digitalWrite(cPin7segSimpleLedOrange, false);
    }
    else if(u8Led == 2)
    {
        m_bSimpleLedGreenUSed = true;
        digitalWrite(cPin7segSimpleLedGreen, false);
    }
}

/*****************************************************************************
 * Function    : invertLed                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CSimpleLed::invertLed(uint8 u8Led)
{
    if(u8Led == 0)
    {
        m_bSimpleLedRedUSed = true;
        if(digitalRead(cPin7segSimpleLedRed))
            digitalWrite(cPin7segSimpleLedRed, false);
        else
            digitalWrite(cPin7segSimpleLedRed, true);
    }
    else if(u8Led == 1)
    {
        m_bSimpleLedOrangeUSed = true;
        if(digitalRead(cPin7segSimpleLedOrange))
            digitalWrite(cPin7segSimpleLedOrange, false);
        else
            digitalWrite(cPin7segSimpleLedOrange, true);
    }
    else if(u8Led == 2)
    {
        m_bSimpleLedGreenUSed = true;
        if(digitalRead(cPin7segSimpleLedGreen))
            digitalWrite(cPin7segSimpleLedGreen, false);
        else
            digitalWrite(cPin7segSimpleLedGreen, true);
    }
}
