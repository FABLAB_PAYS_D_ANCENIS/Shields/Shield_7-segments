#ifndef CSIMPLELED_H
#define CSIMPLELED_H

#include "Shield7Segments.h"

extern char m_bSimpleLedUSed;
extern char m_bSimpleLedGreenUSed;
extern char m_bSimpleLedOrangeUSed;
extern char m_bSimpleLedRedUSed;


class CSimpleLed
{
public:
    CSimpleLed();
    void init(void);
    void switchOnLed(uint8 u8Led);
    void switchOffLed(uint8 u8Led);
    void invertLed(uint8 u8Led);

private:


};

#endif // CSIMPLELED_H
