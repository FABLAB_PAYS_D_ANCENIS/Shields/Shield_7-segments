#include "CBuzzer.h"

uint8 CBuzzer::u8FreqTable[7] = {freqDO,freqRE,freqMI,freqFA,freqSOL,freqLA,freqSI};

/*****************************************************************************
 * Function	: CSimpleLed                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CBuzzer::CBuzzer(uint8 u8PinBuzzer)
{
	// Setup pin
	m_u8PinBuzzer = u8PinBuzzer;
	pinMode(m_u8PinBuzzer, OUTPUT);

	// Frequencies of octave 2 declared by default
	m_u8Octave	  = 3;
	m_u8OctaveDef = 2;
}

/*****************************************************************************
 * Function	: switchOnBuzzer                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CBuzzer::playNote(uint8 u8Note)
{
	NewTone(m_u8PinBuzzer, u8FreqTable[u8Note] * pow(2, m_u8Octave - m_u8OctaveDef));
}

/*****************************************************************************
 * Function	: switchOffBuzzer                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CBuzzer::playTimedNote(uint8 u8Note, uint32 u32Time)
{
	NewTone(m_u8PinBuzzer, u8FreqTable[u8Note] * pow(2, m_u8Octave - m_u8OctaveDef), u32Time);
}

/*****************************************************************************
 * Function	: setOctave                                                      *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CBuzzer::setOctave(uint8 u8Oct)
{
	m_u8Octave = u8Oct;
}

/*****************************************************************************
 * Function	: stopNote                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CBuzzer::stopNote(void)
{
	noNewTone(m_u8PinBuzzer);
}
