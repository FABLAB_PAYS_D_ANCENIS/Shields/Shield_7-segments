#include "Shield7Segments.h"
#include "CSystem7Seg.h"

// Types definition
typedef signed char         int8;
typedef unsigned char       uint8;
typedef short               int16;
typedef unsigned short      uint16;
typedef long                int32;
typedef unsigned long       uint32;
typedef unsigned long long  uint64;
