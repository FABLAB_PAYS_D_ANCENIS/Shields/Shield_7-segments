#include "CAff7Seg.h"

/*****************************************************************************
 * Function    : CAff7Seg                                                    *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CAff7Seg::CAff7Seg()
{
    // New class
    SevenSeg = new SevenSegmentTM1637(cPin7SegClock, cPin7SegDio);

    // Initializes the display
    SevenSeg->begin();

    // Set the brightness to 100 %
    SevenSeg->setBacklight(100);

    // Delay
    SevenSeg->setPrintDelay(800);
}

/*****************************************************************************
 * Function    : writeSegments                                               *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CAff7Seg::writeAllSegments(String s8Str)
{
    // Print
    SevenSeg->print(s8Str);
}

/*****************************************************************************
 * Function    : writeSegments                                               *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CAff7Seg::writeAllSegments(int32 s32Val)
{
	// Check number size
	if((s32Val >= 0) && (s32Val <= 9999))
	{  
		writeOneSegment(3, (char)((char)(s32Val%10) + 0x30));

		if(s32Val >= 10)
		{
			 writeOneSegment(2, (s32Val/10)%10 + 0x30);
		}
		else
		{
			writeOneSegment(2, ' ');
			writeOneSegment(1, ' ');
			writeOneSegment(0, ' ');
		}

		if(s32Val >= 100)
		{
			 writeOneSegment(1, (s32Val/100)%10 + 0x30);
		}
		else
		{
			writeOneSegment(1, ' ');
			writeOneSegment(0, ' ');
		}

		if(s32Val >= 1000)
		{
			 writeOneSegment(0, (s32Val/1000)%10 + 0x30);
		}
		else
		{
			writeOneSegment(0, ' ');
		}
	}
	else if ((s32Val >= -999) &&(s32Val <= 0))
  {
    writeOneSegment(3, (char)((char)(-s32Val%10) + 0x30));
    if(s32Val > -10)
    {
        writeOneSegment(2, "-");
        writeOneSegment(1, ' ');
        writeOneSegment(0, ' ');
    }
    else
    {
        writeOneSegment(2, (-s32Val/10)%10 + 0x30);
      
        if(s32Val > -100)
        {
           writeOneSegment(1, '-');
           writeOneSegment(0, ' ');
        }
        else
        {
            writeOneSegment(1, (-s32Val/100)%10 + 0x30);
            writeOneSegment(0, '-');
        }
    }
  }
  else
	{
		// Write big number with scrolling
		writeAllSegments(String(s32Val));
	}
}

/*****************************************************************************
 * Function    : writeOneSegment                                             *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CAff7Seg::writeOneSegment(uint8 u8Seg, char s8Char)
{
    // Print
    SevenSeg->printRaw(SevenSeg->encode(s8Char), u8Seg);
}

/*****************************************************************************
 * Function    : writeOneSegment                                             *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CAff7Seg::writeOneSegment(uint8 u8Seg, char *s8Char)
{
    // Print
    SevenSeg->printRaw(SevenSeg->encode(s8Char[0]), u8Seg);
}

/*****************************************************************************
 * Function    : clearAllSegments                                            *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CAff7Seg::clearAllSegments(void)
{
    // Clear all segments
    SevenSeg->clear();
}

/*****************************************************************************
 * Function    : clearAllSegments                                            *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CAff7Seg::displayDots(bool bOn)
{
	SevenSeg->setColonOn(bOn);
}
/*****************************************************************************
 * Function    : writeLineByLine                                             *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CAff7Seg::writeLineByLine(uint32 u32Lines)
{
    // Print
    uint8_t rawBytes[4];
    rawBytes[0] = (u32Lines >> 21) & 0x7F;
    rawBytes[1] = (u32Lines >> 14) & 0x7F;
    rawBytes[2] = (u32Lines >> 7) & 0x7F;
    rawBytes[3] = (u32Lines) & 0x7F;
  
    // Print lines
    SevenSeg->printRaw(rawBytes);
}
